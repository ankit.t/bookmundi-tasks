const request1 = fetch('https://api.example.com/data', {
  method: 'POST',
  body: JSON.stringify({ name: 'John', age: 25 }),
  headers: {
    'Content-Type': 'application/json'
  }
});

const request2 = fetch('https://api.example.com/data', {
  method: 'POST',
  body: JSON.stringify({ name: 'Jane', age: 30 }),
  headers: {
    'Content-Type': 'application/json'
  }
});

const request3 = fetch('https://api.example.com/data', {
  method: 'POST',
  body: JSON.stringify({ name: 'Jack', age: 35 }),
  headers: {
    'Content-Type': 'application/json'
  }
});

Promise.all([request1, request2, request3])
  .then(responses => {
    console.log('All requests sent concurrently.');
  })
  .catch(error => {
    console.error(error);
  });
