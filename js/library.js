var library = (function() {

    // Private methods
    function changeClass(element, newClass) {
      element.classList.add(newClass);
    }
  
    function getDataset(element) {
      return element.dataset;
    }
  
    function injectElement(parent, element) {
      parent.appendChild(element);
    }
  
    function makeRequest(url, method, data, successCallback, errorCallback) {
      var xhr = new XMLHttpRequest();
      xhr.open(method, url, true);
      xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            successCallback(xhr.responseText);
          } else {
            errorCallback(xhr.statusText);
          }
        }
      };
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.send(JSON.stringify(data));
    }
  
    function setValue(elem, value) {
      elem.value = value;
    }
  
    function getValue(elem) {
      return elem.value;
    }

    return {
      changeClass: function(element, newClass) {
        changeClass(element, newClass);
      },
      getDataset: function(element) {
        return getDataset(element);
      },
      injectElement: function(parent, elem) {
        injectElement(parent, elem);
      },
      makeRequest: function(url, method, data, successCallback, errorCallback) {
        makeRequest(url, method, data, successCallback, errorCallback);
      },
      setValue: function(elem, value) {
        setValue(elem, value);
      },
      getValue: function(elem) {
        return getValue(elem);
      }
    };
  
  })();
  