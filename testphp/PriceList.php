<?php
class PriceList {
  protected $ids;
  protected $prices;
  
  public function __construct($ids, $prices) {
    $this->ids = $ids;
    $this->prices = $prices;
  }
  
  public function filterByPrice($threshold) {
    $filteredIds = array();
    $filteredPrices = array();
    
    foreach ($this->prices as $i => $price) {
      if ($price > $threshold) {
        $filteredIds[] = $this->ids[$i];
        $filteredPrices[] = $price;
      }
    }
    
    return new PriceList($filteredIds, $filteredPrices);
  }
  
  public function sumPrices() {
    return array_sum($this->prices);
  }
}
?>