<?php 
class ActionController {
    public function processRequest() {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $output = ['error' => 'Invalid request method'];
            echo json_encode($output);
            return;
        }
        $id = $_POST['id'];
        $value = $_POST['value'];

        if (!is_numeric($id)) {
            $output = ['error' => 'Invalid ID'];
            echo json_encode($output);
            return;
        }

        if (!is_string($value)) {
            $output = ['error' => 'Invalid value'];
            echo json_encode($output);
            return;
        }
        $data = array(
            'id' => $id,
            'Value' => $value,
        );
        $output = json_encode($data);
        echo $output;
    }
}
$actionController = new ActionController();
$actionController->processRequest();
?>