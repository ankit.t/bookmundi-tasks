<?php
    class Database {
        private $host;
        private $username;
        private $password;
        private $database;
    
        public function __construct($host, $username, $password, $database) {
            $this->host = $host;
            $this->username = $username;
            $this->password = $password;
            $this->database = $database;
        }
    
        public function connect() {
            try {
                $pdo = new PDO("mysql:host=$this->host;dbname=$this->database", $this->username, $this->password);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $pdo;
            } catch (PDOException $e) {
                echo "Connection failed: " . $e->getMessage();
            }
        }
    
        public function getTableData($tableName) {
            $pdo = $this->connect();
            $query = "SELECT * FROM $tableName";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }
    }

    $database = new Database('localhost', 'root', '', 'db_test');
    $data = $database->getTableData('tests');
    echo json_encode($data);
?>